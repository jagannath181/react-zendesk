// Dependency Imports
import React from 'react';
import PropTypes from "prop-types"; // Styling

import '../../static/scss/App.scss'; // Components

import NoQueries from '../../components/NoQuery';
import _Write_US_Modal from '../../components/WriteUsModal';
import CHAT_PAGE from '../../components/chat'; // Constants

import { PRIORITY_LEVEL_VALUE } from '../../config'; // Api call services

import { CREATE_NEW_ISSUE, GET_ALL_TICKETS, REPLY_TO_COMMENT } from '../../services/zendex';
import Loader from '../../components/loader';

class LandingIndex extends React.Component {
  constructor() {
    super();

    this.addIssue = props => {
      this.setState({
        openAddIssueModal: true
      });
    };

    this.toggle = event => {
      this.setState({
        openAddIssueModal: !this.state.openAddIssueModal
      });
    };

    this.selectPriority = event_name => {
      if (PRIORITY_LEVEL_VALUE[event_name]) this.setState({
        setPriorityLevel: PRIORITY_LEVEL_VALUE[event_name]
      });
    };

    this.queryHandler = event => {
      this.setState({
        userQuery: event.target.value
      });
    };

    this.submitQuery = async () => {
      try {
        if (this.state.userQuery && this.state.setPriorityLevel) {
          const newIssue = {
            subject: this.state.userQuery,
            body: this.state.userQuery,
            priority: this.state.setPriorityLevel,
            status: "new",
            type: "question",
            requester_id: this.state.zendexId.toString()
          };

          if (newIssue.subject && newIssue.requester_id && newIssue.priority) {
            this.setState({
              loader: true
            });
            CREATE_NEW_ISSUE(newIssue.subject, newIssue.body, newIssue.status, newIssue.priority, newIssue.type, newIssue.requester_id).then(async data => {
              this.setState({
                openAddIssueModal: false
              });
              await this.getAllTickets(this.state.email);
            });
          }
        }
      } catch (error) {
        console.log("ERROR::PAGES_INDEX::ERROR:submitQuery", error);
      }
    };

    this.getAllTickets = async email => {
      try {
        const response = await GET_ALL_TICKETS(email);

        if (response?.data?.data) {
          const tickets = response.data.data.open.concat(response.data.data.close);
          if (tickets.length) this.setState({
            queries: tickets,
            selectedTicketId: tickets[0].id,
            loader: false
          });
        }
      } catch (error) {
        console.log("ERROR::INDEX_PAGE::getAllTickets::ERROR", error);
      }
    };

    this.ticketClickHandler = id => {
      this.setState({
        selectedTicketId: id
      });
    };

    this.chatHandler = (event, key) => {
      try {
        if (key && key === 'Enter') {
          this.setState({
            loader: true
          });
          REPLY_TO_COMMENT(this.state.selectedTicketId.toString(), this.state.reply, this.state.zendexId.toString()).then(data => {
            this.setState({
              reply: null,
              loader: false
            });
          }).catch(error => {
            this.setState({
              loader: false
            });
          });
        } else {
          this.setState({
            reply: event.target.value
          });
        }
      } catch (error) {
        console.log("ERROR:::PAGE_INDEX::chatHandler:ERROR", error);
      }
    };

    this.state = {
      openAddIssueModal: false,
      setPriorityLevel: null,
      userQuery: '',
      queries: null,
      selectedTicketId: null,
      ticketHistory: null,
      zendexId: 386277634157,
      reply: null,
      email: "grocerappscrip@gmail.com",
      loader: true
    };
  }

  // One First Mount
  async componentDidMount() {
    try {
      this.getAllTickets(this.state.email);
    } catch (error) {
      console.log("ERROR::INDEX_PAGE::componentDidMount::ERROR", error);
    }
  }

  // On Every UnMount
  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = (state, callback) => {
      return;
    };
  }

  render() {
    return <div className="mainBodyPart">
        {
        /* Body Section */
      }
        {!this.state?.queries?.length ? <NoQueries addIssue={this.addIssue} /> : <CHAT_PAGE loader={this.state.loader} reply={this.state.reply} chatHandler={this.chatHandler} zendexId={this.state.zendexId} ticketHistory={this.state.ticketHistory} ticketClickHandler={this.ticketClickHandler} selectedTicketId={this.state.selectedTicketId} queries={this.state.queries} addIssue={this.addIssue} />}

        {
        /* WRITE_US_MODAL */
      }
        {this.state.openAddIssueModal && <_Write_US_Modal submitQuery={this.submitQuery} queryHandler={this.queryHandler} selectPriority={this.selectPriority} width={"40%"} isOpen={this.state.openAddIssueModal} toggle={this.toggle} />}
      </div>;
  }

} // Create validations for Props, required or type etc.


LandingIndex.propTypes = {
  dispatch: PropTypes.any.isRequired,
  lang: PropTypes.any.isRequired
};
export default LandingIndex; // <!-- Start of ppp9986586 Zendesk Widget script -->
// <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=ca001052-96e8-4559-84cb-071cb9bbd3bb"> </script>
// <!-- End of ppp9986586 Zendesk Widget script -->

{
  /* <Zendesk zendeskKey="ca001052-96e8-4559-84cb-071cb9bbd3bb" defer={true} /> */
}