/* eslint-disable semi */
// Dependency Imports
// eslint-disable-next-line no-unused-vars
import React from 'react'; // Custom Imports

import '../../static/scss/App.scss';
import '../../static/scss/search/search.scss'; // eslint-disable-next-line no-unused-vars

import { APP_NAME } from '../../config'; // eslint-disable-next-line no-unused-vars

import CustomInput from '../../components/InputBox/Input.jsx';
import MainHeader from '../../containers/headers/mainHeader.jsx';

class SearchPage extends React.Component {
  constructor(...args) {
    super(...args);

    this.handleInputChange = event => {
      console.log("this.props..", this.props, event.target.value);
    };
  }

  render() {
    return <>
        {
        /* Header Section */
      }
        <div className="App">
          <MainHeader>
            <h3 className="headerMainTitle">Welcome to Search Portal, by {APP_NAME}.</h3>

            <p>
              <CustomInput id="searchBoxSPage" className="searchBox" onChange={this.handleInputChange} placeholder="Type Here.." />
            </p>
          </MainHeader>
        </div>

        {
        /* Body Section */
      }
        <div className="mainBodyPart">

        </div>

      </>;
  }

}

export default SearchPage;