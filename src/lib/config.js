import LOGO from '../static/images/app_images/appscrip.png';
import WEBP_1 from '../static/images/dummy/1.webp';
import CHAT from "../static/images/app_images/messenger.png";
import QUERIES from "../static/images/app_images/no-queries.png";
import GALLERY from "../static/images/app_images/default-gallery.png";
import CANCEL from "../static/images/app_images/cancel.png";
import SEARCH from "../static/images/app_images/search.png";
import CLIP from "../static/images/app_images/clip.png";
import HAPPINESS from "../static/images/app_images/happiness.png";

export const API_HOST = 'https://api.shoppd.net/v1/zendesk'
export const APP_NAME = 'Appscrip'
export const MAIN_LOGO = LOGO
export const CHAT_IMAGE = CHAT;
export const NO_QUERIES = QUERIES;
export const DEFAULT_GALLERY = GALLERY;
export const CANCEL_IMAGE = CANCEL;
export const SEARCH_IMAGE = SEARCH;
export const CLIP_IMAGE = CLIP;
export const HAPPINESS_IMAGE = HAPPINESS;

// Webp
export const WEBP_DUMMY_1 = WEBP_1

// OG DATA VARS
export const OG_IMAGE = 'https://www.appscrip.com/wp-content/uploads/2019/03/Group-4612.png'

// export priority level value

export const PRIORITY_LEVEL_VALUE = {
    URGENT: "urgent",
    HIGH: "high",
    NORMAL: "normal",
    LOW: 'low'
}
