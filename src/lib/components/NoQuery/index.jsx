import React from "react";
import { CHAT_IMAGE } from '../../config';
import PropTypes from "prop-types";
import "../../static/scss/noquery/noquery.scss";

const NoQueries = (props) => {

    return (
        <div className="default__scr">
            <h5>Help</h5>
            <div className="no__data__found">
                <div>
                    <img src={CHAT_IMAGE} width={60} height={60} className="chat__img" alt />
                </div>
                <h6 className="sub__hdr">No queries found</h6>
                <p className="txt__desc">There is no issues raised</p>
                <button className="add__address__outline" onClick={props.addIssue}>+Add issues</button>
            </div>
        </div>
    );
}


NoQueries.propTypes = {
    addIssue: PropTypes.func.isRequired
  }

export default NoQueries;