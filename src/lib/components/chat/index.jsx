import React, { useEffect, useState } from "react";
import moment from "moment";
import { CLIP_IMAGE, HAPPINESS_IMAGE, SEARCH_IMAGE } from '../../config';
import "../../static/scss/chat/chat.scss";
import { GET_TICKET_HISTORY } from '../../services/zendex';
import Loader from "../../components/loader";
import axios from 'axios';

const CHAT_PAGE = (props) => {

    const [ticketHistory, setTicketHistory] = useState(null);
    const [loader, setLoader] = useState(true);


    useEffect(() => {
        try {
            const source = axios.CancelToken.source()
            const fetchData = async (id) => {
                try {
                    setLoader(true)
                    const response = await GET_TICKET_HISTORY(id);
                    if (response?.data?.data) {
                        setTicketHistory({ ...response.data.data });
                        setLoader(false)
                    }
                } catch (error) {
                    console.log("ERROR::Component::CHAT::UseEffect::ERROR", error);
                    setLoader(false)
                    if (axios.isCancel(error)) {
                    } else {
                        throw error
                    }
                }
            }

            if (!props.reply) {
                fetchData(props.selectedTicketId)
            }

            return () => {
                source.cancel()
            }
        } catch (error) {
            console.log('ERROR IN Component::CHAT::index.js::ERROR', error);
        }
    }, [props.selectedTicketId, props.reply])

    return (
        <div className="chat__scr">
            <div className="border-bottom py-3">
                <div className="col-12">
                    <div className="row align-items-center justify-content-between">
                        <div className="col-auto">
                            <h5 className="mb-0 text-center sub__hdr">Help</h5>
                        </div>
                        <div className="col-auto">
                            <button className="add__issue" onClick={props.addIssue}>+Add issues</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-12 chatUI">
                <div className="row h-100">
                    <div className="col-auto pills__section border-right">
                        <div className="row border-bottom py-3 mb-3">
                            <div className="col-12">
                                <div className="position-relative">
                                    <input type="text" className="form-control inpt__el__search" placeholder="Search" />
                                    <img src={SEARCH_IMAGE} width={16} className="inpt__search__icon" alt="search" />
                                </div>
                            </div>
                        </div>
                        <div className="row d-block">
                            <ul className="nav flex-column issueUL">
                                {props?.queries?.length ? props.queries.map((ticket, index) => (
                                    <li onClick={() => props.ticketClickHandler(ticket.id)} className={`nav-item ${ticket.id == props.selectedTicketId ? 'active' : ''}`} key={index}>
                                        <div>
                                            <p className={ticket?.status?.toLowerCase() !== "closed" ? "mb-1 issue-staus" : "mb-1 issue-staus issue_resolved"}>{ticket.status.toUpperCase()}</p>
                                            <div className="issue-identity">Issue ID: {ticket.id}</div>
                                            <div className="issue-timeStamp">{moment(ticket.timestamp).format('lll')}</div>
                                        </div>
                                    </li>
                                )) : ''}
                            </ul>
                        </div>
                    </div>
                    <div className="col chat__section border-right">
                        <div className="row border-bottom py-3 mb-3">
                            <div className="col">
                                <div className="issue-identity">Issue ID: {ticketHistory?.ticket_id || ''}</div>
                                <div className="issue-timeStamp mb-2">{ticketHistory?.timeStamp ? moment(ticketHistory.timeStamp).format('lll') : ''}</div>
                                {ticketHistory?.subject && <div className="issue-identity">
                                    {ticketHistory.subject}
                                </div>}
                            </div>
                            <div className="col-auto">
                                <p className={ticketHistory?.type?.toLowerCase() !== "closed"  ? "mb-0 issue-staus" : "mb-0 issue-staus issue_resolved"}>{ticketHistory?.type?.toUpperCase() || 'OPEN'}</p>
                            </div>
                        </div>
                        {
                            ticketHistory?.events?.length && ticketHistory?.events.map((event, index) => {
                                if (event?.author_id !== props.zendexId) {
                                    return (
                                        <div className="msg-in-lyt" key={index}>
                                            <div className="msg-in">{event?.body || ""}</div>
                                        </div>
                                    )
                                } else {
                                    return (
                                        <div className="msg-send-lyt" key={index}>
                                            <div className="msg-send">{event?.body || ""}</div>
                                        </div>
                                    )
                                }
                            })
                        }

                        <div className="inpt__send__issue__sec">
                            {ticketHistory?.type?.toLowerCase() !== 'open'
                                ? <div className="issue_resolved_div"> <h6 className="issue_resolved" >Your issue has been resolved</h6> </div>
                                : <>
                                    <input value={props?.reply || ""} onKeyPress={(e) => e.key === "Enter" ? props.chatHandler(null, e.key) : ''} type="text" onChange={(event) => props.chatHandler(event, null)} className="form-control inpt__send__el" placeholder="Type a message" />
                                    <img src={HAPPINESS_IMAGE} width={16} className="inpt__smiley__icon" alt="happines" />
                                    <img src={CLIP_IMAGE} width={16} className="inpt__clip__icon" alt="clip" />
                                </>}
                        </div>
                    </div>
                </div>
            </div>
            {loader || props.loader ? <Loader /> : ""}
        </div>
    )
}

export default CHAT_PAGE;
